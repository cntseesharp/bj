﻿using BJ.Game;
using BJ.Player;
using System.Collections.Generic;
using System.Linq;

namespace BJ.Logic
{
    public class Hand
    {
        public PlayerBase Owner { get; private set; }
        public int Bet { get; set; }

        public Hand(PlayerBase player)
        {
            Owner = player;
        }

        public Hand(Hand baseHand, Card card)
        {
            Owner = baseHand.Owner;
            Bet = baseHand.Bet;
            _hand.Add(card);
        }

        public int Weight
        {
            get
            {
                int weight = 0;
                foreach (Card c in _hand.OrderBy(x => x.Price))
                {
                    weight += c.Price;
                    if (c.Value == 'A' && weight > 21) // 1/11 correction
                        weight -= 10;
                }
                return weight;
            }
        }
        List<Card> _hand = new List<Card>();

        public void AddCard(Card card)
        {
            _hand.Add(card);
        }

        public List<Card> PeekCards { get { return _hand; } }
    }
}
