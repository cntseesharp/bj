﻿using BJ.Player;
using BJ.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BJ.Logic
{
    class Table : IDisposable
    {
        //Локалы с _
        List<PlayerBase> _players = new List<PlayerBase>();
        List<Hand> _hands = new List<Hand>();
        Deck _deck = new Deck(3);
        Hand _dealerHand = new Hand(null);

        bool _started = false;
        bool _processing = true;

        const int _dealersGoal = 17;
        const int _blackjack = 21;
        const int _minimumBet = 2;

        public void AddPlayer(PlayerBase p)
        {
            _players.Add(p);
        }

        public void RemovePlayer(PlayerBase p)
        {
            _players.Remove(p);
        }

        public void StartRotations()
        {
            if (_started) return;
            _started = true;
            new Task(() => { while (_processing) Rotate(); }).Start();
        }

        public void StopRotations()
        {
            _processing = false;
        }

        void Rotate()
        {
            foreach (PlayerBase p in _players)
                _hands.Add(new Hand(p as UserControlled));
            _dealerHand = new Hand(null);

            AskBets();
            DealCards();
            Controller.Interface.DrawTable(_dealerHand, _players, _hands);
            FirstIteration();
            Controller.Interface.DrawTable(_dealerHand, _players, _hands);
            SecondIteration();
            while (_dealerHand.Weight < _dealersGoal)
                _dealerHand.AddCard(_deck.DrawCard());
            Payout();
            Controller.Interface.DrawTable(_dealerHand, _players, _hands);
            TakeCards();
        }

        void FirstIteration()
        {
            List<Hand> toRemove = new List<Hand>();
            List<Hand> toAdd = new List<Hand>();
            foreach (Hand hand in _hands)
            {
                Controller.Interface.DrawTable(_dealerHand, _players, _hands, hand);
                if (hand.Weight == _blackjack && _dealerHand.PeekCards[0].Value != 'A')
                {
                    toRemove.Add(hand);
                    _deck.EnqueueCards(hand.PeekCards);
                    hand.Owner.AddChips((int)(hand.Bet * 1.5));
                    continue;
                }

                //Split
                if (hand.PeekCards[0].Value == hand.PeekCards[1].Value)
                    if (hand.Owner.Chips >= hand.Bet && hand.Owner.AskForSplit())
                    {
                        List<Hand> split = new List<Hand>()
                        {
                            new Hand(hand, hand.PeekCards[0]),
                            new Hand(hand, hand.PeekCards[1]),
                        };

                        hand.Owner.RemoveChips(hand.Bet);
                        toAdd.InsertRange(0, split);
                        toRemove.Add(hand);
                    }
            }

            foreach (Hand h in toRemove)
                _hands.Remove(h);

            foreach (Hand h in toAdd)
                _hands.Add(h);
        }

        void SecondIteration()
        {
            foreach (Hand hand in _hands.Where(x => x.Weight != _blackjack))
            {
                Controller.Interface.DrawTable(_dealerHand, _players, _hands, hand);
                while ((hand.Owner).AskForAction())
                {
                    hand.AddCard(_deck.DrawCard());
                    Controller.Interface.DrawTable(_dealerHand, _players, _hands, hand);
                    if (hand.Weight >= _blackjack)
                        break;
                }
            }
        }

        void Payout()
        {
            int payoutValue = _dealerHand.Weight > _blackjack ? -1 : _dealerHand.Weight;
            foreach (Hand h in _hands.Where(x => x.Weight <= _blackjack)) // => :D <=
            {
                if (h.Weight == payoutValue)
                    h.Owner.AddChips(h.Bet);
                if (h.Weight > payoutValue)
                    h.Owner.AddChips((int)(h.Bet * 1.5));
            }
        }

        void AskBets()
        {
            for (int i = 0; i < _hands.Count; i++)
            {
                Hand h = _hands[i];
                int bet = h.Owner.AskForBet();
                while (bet > h.Owner.Chips) bet = h.Owner.AskForBet();

                if (bet < _minimumBet)
                {
                    _hands.Remove(h);
                    i--;
                    continue;
                }

                h.Bet = bet;
                h.Owner.RemoveChips(bet);
            }
        }

        void DealCards()
        {
            foreach (Hand h in _hands)
            {
                h.AddCard(_deck.DrawCard());
                h.AddCard(_deck.DrawCard());
            }
            _dealerHand.AddCard(_deck.DrawCard());
        }

        void TakeCards()
        {
            foreach (Hand h in _hands)
                _deck.EnqueueCards(h.PeekCards);
            _hands.Clear();
            _deck.EnqueueCards(_dealerHand.PeekCards);
            _dealerHand = null;
        }

        public void Dispose()
        {
            _processing = false;
        }
    }
}
