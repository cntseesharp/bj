﻿using BJ.Game;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BJ.Logic
{
    public class Deck
    {
        Queue<Card> _cards = new Queue<Card>();

        public Deck(int decks = 1, bool shuffle = true)
        {
            if (decks < 1)
                throw new ArgumentException("Decks amount shouldn't be less than 1");

            //Deck init
            for (int deck = 0; deck < decks; deck++)
                foreach (char suit in "♥♦♣♠")
                {
                    int price = 2;
                    foreach (char value in "23456789")
                        _cards.Enqueue(new Card() { Price = price++, Suit = suit, Value = value });
                    foreach (char value in "JQK")
                        _cards.Enqueue(new Card() { Price = price, Suit = suit, Value = value });
                    _cards.Enqueue(new Card() { Price = 11, Suit = suit, Value = 'A' });
                }

            if (shuffle)
                ShuffleDeckCards();
        }

        public void EnqueueCards(List<Card> hand)
        {
            foreach (Card card in hand)
                _cards.Enqueue(card);
        }

        public Card DrawCard()
        {
            return _cards.Dequeue();
        }

        public void ShuffleDeckCards()
        {
            List<Card> storage = _cards.ToList();

            // Fisher-Yates shuffle method 
            int n = storage.Count;
            var rnd = new Random();
            while (n > 1)
            {
                int k = (rnd.Next(0, n) % n);
                n--;
                Card value = storage[k];
                storage[k] = storage[n];
                storage[n] = value;
            }

            _cards.Clear();
            foreach (Card card in storage)
                _cards.Enqueue(card);
        }
    }
}
