﻿namespace BJ.Player
{
    public abstract class PlayerBase
    {
        public string Name { get; protected set; }
        public int Chips { get; protected set; }

        public abstract int AskForBet(); //Asks for a bet
        public abstract bool AskForSplit();
        public abstract bool AskForAction();

        public void AddChips(int chipsCount)
        {
            Chips += chipsCount;
        }
        public void RemoveChips(int chipsCount)
        {
            Chips -= chipsCount;
        }
    }
}
