﻿using System;

namespace BJ.Player
{
    class UserControlled : PlayerBase
    {
        public UserControlled(string name, int chips)
        {
            Name = name;
            Chips = chips;
        }

        public override bool AskForAction()
        {
            UI.Controller.Interface.DrawActionRequest(this);
            return Console.ReadLine().Trim() == "1";
        }

        public override int AskForBet()
        {
            UI.Controller.Interface.DrawBetRequest(this);
            int result = 0;
            while (!int.TryParse(Console.ReadLine(), out result)) ;
            return result;
        }

        public override bool AskForSplit()
        {
            UI.Controller.Interface.DrawSplitRequest(this);
            return Console.ReadLine().Trim() == "y";
        }
    }
}
