﻿using BJ.Logic;
using BJ.Player;

namespace BJ
{
    class Program
    {
        static void Main(string[] args)
        {
            UI.Controller.Init(new UI.ConsoleFormatter());

            Table table = new Table();

            table.AddPlayer(new UserControlled("Arc", 200));
            table.AddPlayer(new UserControlled("CntSee", 200));
            table.StartRotations();

            while (true) ;
        }
    }
}
