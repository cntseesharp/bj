﻿namespace BJ.Game
{
    public class Card
    {
        public char Suit  { get; set; } // ♥♦♣♠
        public char Value { get; set; } // 2-9 J  Q  K  A
        public int  Price { get; set; } // 2-9 10 11 12 13
    }
}
