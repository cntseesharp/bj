﻿using BJ.Logic;
using BJ.Player;
using System.Collections.Generic;

namespace BJ.UI
{
    public interface IUserInterface
    {
        void DrawTable(Hand dealerHand, List<PlayerBase> players, List<Hand> hands, Hand currentHand = null);
        void DrawBetRequest(PlayerBase player);
        void DrawActionRequest(PlayerBase player);
        void DrawSplitRequest(PlayerBase player);
    }
}
