﻿using BJ.Game;
using BJ.Logic;
using BJ.Player;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BJ.UI
{
    public class ConsoleFormatter : IUserInterface
    {
        public void DrawTable(Hand dealerHand, List<PlayerBase> players, List<Hand> hands, Hand currentHand = null)
        {
            Console.Clear();
            Console.WriteLine("Dealer");
            foreach (Card card in dealerHand.PeekCards)
                DrawCard(card);
            Console.Write("\b \b - " + dealerHand.Weight + "\r\n");

            Console.ForegroundColor = ConsoleColor.Gray;
            foreach (PlayerBase player in players)
            {
                Console.WriteLine(player.Name);
                foreach (Hand hand in hands.Where(x => x.Owner == player))
                {
                    foreach (Card card in hand.PeekCards)
                        DrawCard(card);
                    Console.Write("\b \b - " + hand.Weight + (hand == currentHand ? " <---" : "") + "\r\n");
                }
            }
            Console.WriteLine();
        }

        public void DrawCard(Card card)
        {
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Red;
            if (card.Suit == '♣' || card.Suit == '♠')
                Console.ForegroundColor = ConsoleColor.Black;
            Console.Write(card.Value + "" + card.Suit);
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write('|');
        }

        public void DrawBetRequest(PlayerBase player)
        {
            Console.WriteLine("Balance: " + player.Chips + ". Place your bet, " + player.Name);
        }

        public void DrawActionRequest(PlayerBase player)
        {
            Console.WriteLine("1 - Hit me\r\n2 - Stand");
        }

        public void DrawSplitRequest(PlayerBase player)
        {
            Console.WriteLine("Wanna split? (y\\n)");
        }
    }
}
