﻿using System;

namespace BJ.UI
{
    public static class Controller
    {
        static IUserInterface _uiController = null;
        public static void Init(IUserInterface iface)
        {
            _uiController = iface;
        }

        public static IUserInterface Interface
        {
            get
            {
                if (_uiController == null)
                    throw new Exception("You should initialize controller before using");
                return _uiController;
            }
        }
    }
}
